# frozen_string_literal: true

require 'test_helper'

class ArmstrongControllerTest < ActionDispatch::IntegrationTest
  test 'should get input' do
    get armstrong_path
    assert_response :redirect
  end

  test 'should get result' do
    get armstrong_input_path
    assert_response :success
  end

  test('should get 548834 for view with 6') do
    get result_armstrong_path, params: {number: 6}
    assert_equal assigns[:result], [548834]
  end

  test('should get [] for view with 2') do
    get result_armstrong_path, params: {number: 2}
    assert_equal assigns[:result], []
  end
end
